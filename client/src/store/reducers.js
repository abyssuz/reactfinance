import { SET_TICKERS, SORT_BY_PRICE } from './types';

const initialState = {
  tickers: [],
  sortByPrice: '',
};

export function rootReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_TICKERS:
      if (state.tickers.length > 0) {
        payload = payload.map((newTicker) => {
          const prevTicker = state.tickers.find(
            (prevTicker) => prevTicker.id === newTicker.id
          );
          return newTicker.price > prevTicker.price
            ? { ...newTicker, priceChange: 'up' }
            : { ...newTicker, priceChange: 'down' };
        });
      }

      if (state.sortByPrice === 'desc') {
        payload.sort((a, b) => {
          return b.price - a.price;
        });
      } else if (state.sortByPrice === 'asc') {
        payload.sort((a, b) => {
          return a.price - b.price;
        });
      }

      return { ...state, tickers: payload };

    case SORT_BY_PRICE:
      let sort = '';

      switch (state.sortByPrice) {
        case 'asc':
          sort = 'desc';
          break;
        case 'desc':
          sort = 'asc';
          break;
        case '':
          sort = 'asc';
      }

      if (sort === 'desc') {
        state.tickers.sort((a, b) => {
          return b.price - a.price;
        });
      } else if (sort === 'asc') {
        state.tickers.sort((a, b) => {
          return a.price - b.price;
        });
      }

      return { ...state, sortByPrice: sort };
  }
  return state;
}
