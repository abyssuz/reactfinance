export const FETCH_TICKERS = 'FETCH_TICKERS';
export const SET_TICKERS = 'SET_TICKERS';
export const SORT_BY_PRICE = 'SORT_BY_PRICE';
