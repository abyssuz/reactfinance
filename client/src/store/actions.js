import { SET_TICKERS, SORT_BY_PRICE  } from './types.js';

export function setTickers(payload) {
    return { type: SET_TICKERS, payload };
}

export function sortByPrice() {
    return { type: SORT_BY_PRICE };
}

