import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setTickers, sortByPrice } from './store/actions';
import fetchData from './assets/FetchData';
import Table from './Components/Table';
import './style.css';

const App = ({ tickers, setTickers, sortByPrice }) => {
  const sortCb = () => {
    sortByPrice();
  }

  useEffect(() => {
    fetchData(setTickers);
  }, []);

  return (
    <div className="container">
      <Table tickers={tickers} sortCb={sortCb}/>
    </div>
  );
};

const mapStateToProps = (state) => {
  if (state.tickers === undefined) {
    return { tickers: [] };
  }
  return state;
};

const mapDispatchToProps = (dispatch) => {
  return {
    setTickers: bindActionCreators(setTickers, dispatch),
    sortByPrice: bindActionCreators(sortByPrice, dispatch),
  };
};

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

export default ConnectedApp;
