import ioClient from 'socket.io-client';

const ENDPOINT = 'localhost:4000';

const fetchData = (setTickers) => {
  const socket = ioClient(ENDPOINT);
  socket.emit('start');
  socket.on('ticker', (item) => {
    const response = Array.isArray(item) ? item : [item];

    response.forEach((item) => {
      for (let key in item) {
        if (!isNaN(item[key])) {
          item[key] = parseFloat(item[key]);
        }
      }
    });
    setTickers(response);
  });
  return () => socket.disconnect();
};

export default fetchData;
