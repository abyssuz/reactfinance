import React from 'react';

const Table = ({ tickers, sortCb }) => {
  return (
    <div className="table-box">
      <table>
        <thead>
          <tr>
            <th>Ticker</th>
            <th>Exchange</th>
            <th onClick={() => sortCb()}>Price</th>
            <th>Change</th>
            <th>Change percent</th>
            <th>Dividend</th>
            <th>Yield</th>
            <th>Last trade</th>
          </tr>
        </thead>
        <tbody>
          {tickers.map((ticker) => {
            return (
              <tr key={ticker.id}>
                <td>{ticker.ticker}</td>
                <td>{ticker.exchange}</td>
                <td>
                  <span
                    className={
                      ticker.priceChange !== undefined
                        ? ticker.priceChange === 'up'
                          ? 'up'
                          : 'down'
                        : ''
                    }
                  >
                    {ticker.price}
                  </span>
                </td>
                <td>{ticker.change}</td>
                <td>{ticker.change_percent}</td>
                <td>{ticker.dividend}</td>
                <td>{ticker.yield}</td>
                <td>{ticker.last_trade_time}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
